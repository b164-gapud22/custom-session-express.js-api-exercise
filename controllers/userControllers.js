const User = require('../model/users');

// Encrypted password
const bcrypt = require('bcrypt');
const auth = require("../auth")

/*
User registration
1. Create a new user object
2. Make sure that the password is encrypted.
3. Save the new user to the database.

*/

module.exports.userRegistration = (reqBody) => {

	// Create new user
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	// Save new user
	return newUser.save().then((newUser, error) => {
		// If error
		if(error){
			return false;
		} else {
			// If successfully saved.
			return `${reqBody.firstName} is successfully registered.`
		}
	})

}




/*
User Login
1. Determined if the user's email already exist in database.
2. Compare the password provided in the login with the password in database.
3. Generate a JSON web token if the user successfuly log in and false if not.
*/


module.exports.userAuthentication = (reqBody) => {
	return User.findOne({ email:reqBody.email }).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			}
		}
	})


}

// 	Retrieve all users
module.exports.getUsers = () => {
	return User.find({}).then(result => {
		return result
	})
};


// Retrieve specific user
module.exports.getSpecificUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result, error) => {
		if(error){
			return false
		} else {
			return result;
		}
	})
};