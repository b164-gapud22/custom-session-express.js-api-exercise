// Dependencies
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const cors = require("cors");

// Routes
const userRoutes = require("./routes/userRoutes")


// Server
const app = express();


// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));


// http://localhost:4000/users
app.use("/users", userRoutes);




// Connect to mongoDB
mongoose.connect(process.env.DB_CONNECTION,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log(`Now connected to mongoDB Atlas`));

app.listen(process.env.PORT, () => {
	console.log(`API is now listening to port: ${process.env.PORT}`)
});