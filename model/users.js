/*
MODEL:
1. firstName of the user.
2. lastName of the user.
3. email of the user.
4. password of the user.
5. registration date.
6. is the User an Admin?

*/


const mongoose = require("mongoose");

const userSchema = mongoose.Schema({

	firstName: {
		type: String,
		require: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		require: [true, 'Last name is required.']
	},
	email: {
		type: String,
		require: [true, 'Email is required.']
	},
	password: {
		type: String,
		require: [true, 'Password is required.']
	},
	registrationDate: {
		type: Date,
		default: new Date()
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model("user", userSchema);