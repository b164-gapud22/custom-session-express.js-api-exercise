const express = require("express");
const router = express.Router();
const auth = require("../auth")


const UserController = require("../controllers/userControllers");


// user registration
router.post("/register", (req, res) => {
	UserController.userRegistration(req.body).then(result => {
		res.send(result);
	})
});



// user authentication
router.post("/login", (req, res) => {
	UserController.userAuthentication(req.body).then(result => {
		res.send(result)
	})
});


// Retrieve all users
router.get("/allUsers", (req, res) => {
	UserController.getUsers().then(result => res.send(result))
});


// Retrievef specific user
router.get("/specificUser", (req, res) => {
	UserController.getSpecificUser(req.body).then(result => res.send(result))
});





module.exports = router;